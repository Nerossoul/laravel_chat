<?php
namespace App\Chat;

use App\Chat\Contracts\ChatTopicContract;
use App\Topic;
use App\TopicUser;
use Auth;

class ChatTopic implements ChatTopicContract
{
    public static function all(Topic $topic)
    {
        return $topic::all();
    }

    public static function store($title, $user_id)
    {
        dump('create function');
    }

    public static function getTopicMessages($id)
    {
        return Topic::with('messages', 'users')->where('topics.id', '=', $id)->get();
    }

    public static function isJoined($id)
    {
        $joined = TopicUser::where('user_id', '=', Auth::user()->id)->where('topic_id', '=', $id)->get();
        $joined = count($joined) > 0;
        return $joined;
    }

    public static function getChildMessages($messages, $parent_id)
    {
        $childMessagesArray = [];
        foreach ($messages as $message) {
            if ($message->parent_message_id == $parent_id) {
                $message->children = ChatTopic::getChildMessages($messages, $message->id);
                $childMessagesArray[] = $message;
            }
        }
        return $childMessagesArray;
    }
}
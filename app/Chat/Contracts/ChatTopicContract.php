<?php
namespace App\Chat\Contracts;

use App\Topic;

interface ChatTopicContract
{
    public static function all(Topic $topic);
    public static function store($title, $user_id);
    public static function getTopicMessages($id);
    public static function isJoined($id);
    public static function getChildMessages($messages, $parent_id);

}

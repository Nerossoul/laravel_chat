<?php
namespace App\Chat\Facades;

use Illuminate\Support\Facades\Facade;

class ChTopic extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "App\Chat\Contracts\ChatTopicContract";
    }
}
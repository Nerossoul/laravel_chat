<?php

namespace App\Http\Controllers\Chat;

use App\Http\Controllers\Controller;
use App\Message;
use Auth;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userData = $request->all();
        $newMessage = new Message([
            'topic_id' => $userData['topic_id'],
            'text' => $userData['text'],
            'user_id' => Auth::user()->id,
            'parent_message_id' => $userData['parent_message_id'],
        ]);
        $newMessage->save();
        return redirect('chat/topics/' . $userData['topic_id'] . '#chat_form');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {$usersData = $request->all();

        //TODO user uthorization are needed here but it's just a test :-)
        $message = Message::find($id);
        $message->deleted = $usersData['deleted'];
        $message->save();

        return redirect('chat/topics/' . $usersData['topic']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

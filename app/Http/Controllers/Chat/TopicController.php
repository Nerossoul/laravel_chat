<?php

namespace App\Http\Controllers\Chat;

use App;
use App\Http\Controllers\Controller;
use App\Topic;
use App\TopicUser;
use App\User;
use Auth;
use ChTopic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('chat/topics_all', ['topics' => ChTopic::all(new Topic)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('chat/topic_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userData = $request->all();
        $user = User::find(Auth::user()->id);
        $topic = new Topic(['title' => $userData['title']]);
        $result = $user->own_topics()->save($topic);
        return redirect('chat/topics/' . $result->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $joined = ChTopic::isJoined($id);
        $thisTopicMessages = ChTopic::getTopicMessages($id);

        $messages = $thisTopicMessages->flatMap->messages;
        $messagesTree = ChTopic::getChildMessages($messages, 0);

        $users = $thisTopicMessages->flatMap->users;
        $usersNames = [];
        foreach ($users as $user) {
            $usersNames[$user->id] = $user->name;
        }
        $current_user = Auth::user()->id;
        $topicUsers = TopicUser::where('topic_id', $id)->get();
        $usersStatus = [];
        foreach ($topicUsers as $user) {
            $usersStatus[$user->user_id] = $user->blocked;
        }
        if (!isset($usersStatus[$current_user])) {
            $usersStatus[$current_user] = 0;
        }

        $repyMessageId = $request->input('reply');
        $topicOwner = Auth::user()->id == $thisTopicMessages[0]->user_id ? 1 : 0;

        return view('chat/read_topic', [
            'current_user' => $current_user,
            'messagesTree' => $messagesTree,
            'users' => $usersNames,
            'user_joined' => $joined,
            'topic_id' => $id,
            'topic_title' => $thisTopicMessages[0]->title,
            'reply' => $repyMessageId,
            'topicOwner' => $topicOwner,
            'usersStatus' => $usersStatus,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }

    public function join($id)
    {
        $user = User::find(Auth::user()->id);
        $topic = Topic::find($id);
        $result = $user->topics()->save($topic);
        return redirect('chat/topics/' . $result->id);

    }
}
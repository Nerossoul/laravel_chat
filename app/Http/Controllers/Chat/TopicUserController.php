<?php

namespace App\Http\Controllers\Chat;

use App\Http\Controllers\Controller;
use App\TopicUser;

class TopicUserController extends Controller
{
    public function banHandler($topic_id, $user_id)
    {
        $topicUser = TopicUser::where('topic_id', $topic_id)->where('user_id', $user_id)->get();
        $userStatus = $topicUser[0]->blocked;
        TopicUser::where('topic_id', $topic_id)->where('user_id', $user_id)->update(['blocked' => !$userStatus]);
        return redirect('chat/topics/' . $topic_id);
    }
}
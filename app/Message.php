<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = array('topic_id',
        'text',
        'user_id',
        'parent_message_id');

    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Get children Messages
    public function children()
    {
        return $this->hasMany(self::class, 'parent_message_id');
    }

}

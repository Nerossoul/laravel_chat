<?php

namespace App\Providers;

use App\Chat\ChatTopic;
use Illuminate\Support\ServiceProvider;

class ChatTopicServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Chat\Contracts\ChatTopicContract', function () {
            return new ChatTopic;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
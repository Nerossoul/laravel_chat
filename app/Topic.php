<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = array('title', 'user_id');
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'topic_user', 'topic_id', 'user_id');
    }
}
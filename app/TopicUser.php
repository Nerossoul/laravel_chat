<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicUser extends Model
{
    protected $table = 'topic_user';
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }
}
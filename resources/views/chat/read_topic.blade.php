@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ $topic_title }}
                    @if ($topicOwner)
                    - You Are Creator
                    @endif
                </div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @include('layouts.messages', ['messages' => $messagesTree, 'reply'=>$reply, 'topicOwner' =>
                    $topicOwner, 'usersStatus' => $usersStatus, 'current_user' => $current_user])
                    @if ($usersStatus[$current_user])
                    <span class='text-danger'>You can't post messages in this topic because You are BANNED!!!</span>
                    @elseif ($user_joined)
                    <form method="post" action="/chat/messages">
                        @csrf
                        <input type="hidden" name="topic_id" value='{{$topic_id}}'>
                        <input type="hidden" name="parent_message_id" value='0'>
                        <div class="form-group row">
                            <label for="message" class="col-md-4 col-form-label text-md-right">Just type</label>

                            <div class="col-md-6">
                                <textarea rows="4" , cols="54" id="message" name="text" style="resize:none "></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Send
                                </button>
                            </div>
                        </div>
                    </form>
                    @else
                    <form method="post" action="/chat/topic/{{$topic_id}}/join">
                        @csrf
                        <button type="submit" class="btn btn-primary">
                            Join
                        </button>
                    </form>
                    @endif
                    <div id="chat_form"></div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
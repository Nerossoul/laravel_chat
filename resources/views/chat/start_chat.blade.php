@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Wellcome to the Laravel Chat</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    You are logged in as {{ Auth::user()->name }}
                    <hr>
                    <form method="get" action="chat/topics">
                        @csrf
                        <button type="submit" class="btn btn-primary">
                            All Topics
                        </button>
                    </form>
                    <form method="get" action="chat/topics/create">
                        @csrf
                        <button type="submit" class="btn btn-primary">
                            Create New Topic
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
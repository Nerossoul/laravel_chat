@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">All Topics</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="get" action="/chat/topics/create">
                        @csrf
                        <button type="submit" class="btn btn-primary">
                            Create New Topic as {{ Auth::user()->name }}
                        </button>
                    </form>
                    <hr>
                    @forelse( $topics as $topic)
                    <a href="/chat/topics/{{ $topic['id'] }}"> {{ $topic['title'] }}</a><br>
                    @empty
                    It is empty list yet.
                    @endforelse
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
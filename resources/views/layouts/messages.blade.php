@foreach ($messages as $message)


{{ $users[$message->user_id] }}
{{ $message->created_at }}

@if ($topicOwner)
<form method="post" action="/chat/topicuser/{{$message->topic_id}}/{{$message->user_id}}" style="display:inline;">
    @csrf
    <input name="_method" type="hidden" value="PUT">
    <button type="submit" class="">
        @if ($usersStatus[$message->user_id]) Unban User @else Ban User @endif
    </button>
</form>
<form method="post" action="/chat/messages/{{ $message->id }}" style="display:inline;">
    @csrf
    <input name="_method" type="hidden" value="PUT">
    <input name="topic" type="hidden" value="{{ $message->topic_id }}">
    <input type="hidden" name="deleted" @if($message->deleted) value='0' @else value='1' @endif>
    <button type="submit" class="">
        @if ($message->deleted) Restore message @else Delete message @endif
    </button>
</form>
@endif<br>
@if($message->deleted) Message deleted @else {{ $message->text }} @endif

<br>
@if ($usersStatus[$current_user])
@elseif ($reply == $message->id)
<form method="post" action="/chat/messages">
    @csrf
    <input type="hidden" name="topic_id" value='{{$topic_id}}'>
    <input type="hidden" name="parent_message_id" value='{{$message->id}}'>
    <div class="form-group row">
        <label for="message" class="col-md-4 col-form-label text-md-right">Type your answer
            here</label>
        <div class="col-md-6">
            <textarea rows="4" , cols="54" id="message" name="text" style="resize:none "></textarea>
        </div>
    </div>
    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Reply
            </button>
            <a href='{{ $message->topic_id }}' class="btn text-primary">
                cancel
            </a>
        </div>
    </div>

</form>

@else

<a href='{{ $message->topic_id }}?reply={{ $message->id }}' class="text-primary">
    reply
</a>

@endif
@if ($message->children)
<div style="margin-left:10px; padding-left:10px; border-left:1px solid #ddd; ">
    @include('layouts.messages', [
    'messages' =>$message->children ,
    'reply'=>$reply,
    'topicOwner' => $topicOwner,
    'usersStatus' => $usersStatus,
    'current_user' => $current_user
    ])
</div>
@endif
<hr>
@endforeach

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'chat', 'middleware' => ['web', 'auth']], function () {
    Route::get('/', 'StartChatController@index')->name('chat');
    Route::resource('topics', 'Chat\TopicController');
    Route::resource('messages', 'Chat\MessageController');
    Route::post('topic/{id}/join', 'Chat\TopicController@join');
    Route::put('topicuser/{topic_id}/{user_id}', 'Chat\TopicUserController@banHandler');
});